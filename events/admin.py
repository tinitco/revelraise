import models
from django.contrib import admin 


class EventAdmin(admin.ModelAdmin):
    list_display = ('evname','descr', 'tier_one', 'status', 'funding_status', 'is_tipped')
    list_filter = ['status', 'is_tipped']
    search_fields = ['evname']
    
admin.site.register(models.Event, EventAdmin)

admin.site.register(models.SingletonSiteParams)

#creator, evname, descr, tier_one, tier_one_post_tipp, tier_one_desc, tier_two, tier_two_post_tipp, 
#tier_two_desc, tier_three, tier_three_post_tipp, tier_three_desc
