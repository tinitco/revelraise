from django.db import models
from django import forms
import datetime
import json
from django.conf import settings
import datetime
import requests

# Create your models here.

from django.contrib.auth.models import User
from django.forms import ModelForm
from time import time
from django.core.validators import MaxLengthValidator
from django.template.defaultfilters import default
from django.forms.extras.widgets import SelectDateWidget
from singleton_models.models import SingletonModel

class SiteSettings(models.Model):
    gateway_comission = models.FloatField(default = 2.6)
    ravel_raise_comission = models.FloatField(default = 8)


def get_upload_file_name(instance, filename):
    return "uploads/%s_%s" % (str(time()).replace('.','_'), filename)




class Event(models.Model):
    STATUS = (
              ('S', 'Submitted'),
              ('A', 'Accepted'),
              ('R', 'Rejected'),
              ('T', 'Tipped'),
              ('F', 'Finished')
              )
    creator = models.ForeignKey(User)
    evname = models.CharField(("evname"),max_length=60)
    descr = models.TextField(("descr"),max_length=500)
    
    tier_one = models.CharField(("tier_one"),max_length=6, blank=True, null=True)
    tier_one_post_tipp = models.CharField(("tier_one_post_tipp"),max_length=6, blank=True, null=True)
    tier_one_desc = models.CharField(("tier_one_desc"),max_length=140, blank=True, null= True)
    tier_one_tickets = models.CharField(("tier_one_tickets"),max_length=3, blank=True, null= True)
    
    tier_two = models.CharField(("tier_two"),max_length=6, blank=True, null=True)
    tier_two_post_tipp = models.CharField(("tier_two_post_tipp"),max_length=6, blank=True, null=True)
    tier_two_desc = models.CharField(("tier_two_desc"),max_length=140, blank=True, null= True)
    tier_two_tickets = models.CharField(("tier_two_tickets"),max_length=3, blank=True, null= True)
    
    tier_three = models.CharField(("tier_three"),max_length=6, blank=True, null=True)
    tier_three_post_tipp = models.CharField(("tier_three_post_tipp"),max_length=6, blank=True, null=True)
    tier_three_desc = models.CharField(("tier_three_desc"),max_length=140, blank=True, null= True)
    tier_three_tickets = models.CharField(("tier_three_tickets"),max_length=3, blank=True, null= True)
    
#    tierOne = models.ForeignKey(Tier,related_name='tier_one')
#    tierTwo = models.ForeignKey(Tier,related_name='tier_two', null = True)
#    tierThree = models.ForeignKey(Tier,related_name='tier_three', null = True)
    
    location = models.CharField(("location"),max_length=100, null= True)
    address = models.CharField(("address"),max_length=100)
    phone = models.CharField(("phone"),max_length=30, blank=True, null= True)
    website = models.CharField(("website"),max_length=100, blank=True, null= True)
    pic = models.FileField(upload_to=get_upload_file_name)
    vid_embed = models.CharField(("website"),max_length=1000)
    
    tipp_point = models.CharField(("tipp_point"),max_length=10)
    last_date = models.DateField(('last_date'))
    status = models.CharField(max_length = 1, choices=STATUS, default = 'S')
    funding_status = models.IntegerField(("status_percent"), default = 0)
    is_tipped = models.BooleanField(default = False)
    
    #@classmethod
    def update_funding_status(self):
        if self.is_tipped:
            self.funding_status = 100
            self.save() 
            return 100
        sum = 0
        for ticket in Ticket.objects.filter(event_id = self):
            sum = sum + float(self.get_tier_price(ticket.event_tier))
        self.funding_status = int(sum*100/float(self.tipp_point))
        if self.funding_status > 100 : self.funding_status = 100
        self.save() 
        
    
    #@classmethod
    def get_tier_price(self, tier_num):
        if tier_num == '1':
            return self.tier_one_post_tipp if self.is_tipped and\
                       self.tier_one_post_tipp else self.tier_one
        elif tier_num =='2' and self.tier_two:
            return self.tier_two_post_tipp if self.is_tipped and\
                       self.tier_two_post_tipp else self.tier_two
        elif self.tier_three:
            return self.tier_three_post_tipp if self.is_tipped and\
                       self.tier_three_post_tipp else self.tier_three
        return self.tier_one
    
    # On Python 3: def __str__(self):
    def __unicode__(self):
        return self.evname


#class Tier(models.Model):
#    tier_price = models.CharField(("tier_price"),max_length=6, blank=True, null=True)
#    tier_price_post_tipp = models.CharField(("tier_price_post_tipp"),max_length=6, blank=True, null=True)
#    tier_desc = models.CharField(("tier_desc"),max_length=140)
#    event = models.ForeignKey(Event)
#    
#    @classmethod
#    def get_price(self):
#        return self.tier_price_post_tipp if self.event.is_tipped else self.tier_price
#    
#    def __unicode__(self):
#        return self.tier_desc    

class EventViews( models.Model ):
    event_id = models.ForeignKey('events.Event', related_name='views')
    count = models.IntegerField(default=0)

    def __unicode__(self):
        return self.count



class Ticket(models.Model):
    STATUS = (
              ('U', 'Uncharged'),
              ('C', 'Charged'),
              ('D', 'Declined')
              )
    holder = models.ForeignKey(User)
    event_id = models.ForeignKey(Event)
    event_tier = models.CharField(("tier_id"),max_length=1)
    balanced_uri = models.CharField(("balanced_uri"),max_length=100)
    card_name = models.CharField(("card_name"),max_length=20)
    card_four_digits = models.CharField(("card_digits"),max_length=4)
    status = models.CharField(max_length = 1, choices=STATUS, default = 'U')
    
    #@classmethod
    def charge(self):
        if self.status != 'U': return
        base_uri = settings.BALANCED_URL
        customer_uri = base_uri+'/v1/customers'
        usr = settings.BALANCED_KEY #'ak-test-2DBryLFR3BBam1CipbWEGSO6gqVOBKghP'
        
        #add a customer 
#        request = urllib2.Request(customer_uri, data = '')
#        response = urllib2.urlopen(request)
#        res_data = json.load(response)
        
        r = requests.post(customer_uri, auth=(usr, ''))
        res_data = r.json()
        
        res_data['uri'] = '/v1/customers/'+res_data['id']
        
        # add a cc token to the customer
        test_card = '/v1/marketplaces/'+settings.BALANCED_MARKET+'/cards/CC3czjpElvHSwwPt276lUk5i'
        values = {'card_uri':self.balanced_uri}
        r = requests.put(base_uri + res_data['uri'], data = values, auth=(usr, ''))
        res_data = r.json()
        if 'status' in res_data.keys():
            pass # something's wrong .. deal with it.
        
        # and finally charge the customer. 
        values = {"appears_on_statement_as":settings.BALANCED_STATEMENT_TEXT,
                  "amount": 50,  #self.event_id.get_tier_price(self.event_tier),
                  "description":"Revel Raise: Ticket for event "+ self.event_id.evname}
        
        res_data['debits_uri'] = '/v1/customers/'+res_data['id']+'/debits'
        
        r = requests.post(base_uri + res_data['debits_uri'], data = values, auth=(usr, ''))
        res_data = r.json()
        if res_data['status'] != 'succeeded':
            self.status = 'D'
            pass # something's wrong .. deal with it.
        else:
            self.status = 'C'
            sale = Sale()
            res_data['transaction_number']
        self.save()

    # On Python 3: def __str__(self):
    def __unicode__(self):
        return str(self.holder) + str(self.event_id) + str(self.event_tier)

    @classmethod
    def create(self, holder, event_id, tier_id, bal_uri, c_four, c_name):
        ticket = self(holder=holder, event_id=event_id, 
                      event_tier=tier_id, balanced_uri = bal_uri,
                      card_name = c_name, card_four_digits = c_four)
        ticket.save()
        return ticket

class Sale(models.Model):
    ticket = models.ForeignKey(Ticket)
    transaction_number = models.CharField(max_length = 15)
    
    def __unicode__(self):
        return self.transaction_number
    
class SiteParams(SingletonModel):
    ravel_raise_comission = models.FloatField()
    payment_gateway_comission = models.FloatField()


class SingletonSiteParams(models.Model):
    ravel_raise_comission = models.FloatField()
    payment_gateway_comission = models.FloatField()