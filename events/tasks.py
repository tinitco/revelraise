from models import Event, Ticket
from celery.decorators import task

@task()
def process_event(event, **kwargs):
    if event.funding_status == 100 and not event.is_tipped:
        for ticket in Ticket.objects.filter(event_id_id = event):
            ticket.charge()
        event.is_tipped = True
        event.save()
