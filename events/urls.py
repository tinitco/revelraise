from django.conf.urls import patterns, url, include

from events import views

from django.views.decorators.csrf import csrf_exempt

from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = patterns('',
    #url(r'^filterCity(?P<city>[\w|\W]+)/$', views.index, name='index'),
    #url(r'^filterVenue(?P<venue>[\w|\W]+)/$', views.index, name='index'),
    url(r'^(?P<event_id>\d+)/$', views.detail, name='events'),
    url(r'createanevent/$', views.createanevent, name='createanevent'),
    url(r'delete/(?P<event_id>\d)$', views.deleteticket, name='deleteticket'),


)
