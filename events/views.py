#from hgext.inotify.linux.watcher import event
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext, loader
from django.http import Http404
from django.shortcuts import render, get_object_or_404, render_to_response, redirect
from django.contrib.auth.decorators import login_required
from django.db.models import F
from django.core.exceptions import ObjectDoesNotExist
from django.core.context_processors import csrf
from django.core.urlresolvers import reverse
from django.views.decorators.csrf import csrf_exempt
from django.forms.models import modelformset_factory
#from django.contrib.gis.geoip import GeoIP
from django.core.mail import EmailMessage

from events.models import Event, Sale, EventViews
from events.forms import SalePaymentForm
from events.models import Event, Ticket, SingletonSiteParams

from hello.forms import EventForm


from tasks import process_event
import json
from allauth.account.forms import LoginForm, SignupForm


#@login_required
#@csrf_protect
def detail(request, event_id):
    try:
        c = {}
        d = []
        event = Event.objects.get(pk=event_id)
        event_tier1 = event.tier_one_tickets
        event_tier2 = event.tier_two_tickets
        event_tier3 = event.tier_three_tickets
        
        ticket = Ticket.objects.filter(event_id_id=event_id)
        ticket_user = Ticket.objects.filter(holder_id=request.user.id, event_id_id = event_id)
        if ticket_user:
            c['exists'] = 'true'

        if event_tier1:
            ticket_tier1 = ticket.filter(event_tier =1).count()
            if int(ticket_tier1) >= int(event_tier1):
                c['tier_one'] = 'No tickets available'
                c['one.error'] = True
            else:
                c['tier_one'] = int(event_tier1)-int(ticket_tier1)
                d.append((1000,'Tier 1'))
            
        if event_tier2:
            ticket_tier2 = ticket.filter(event_tier =2).count()
            if int(ticket_tier2) >= int(event_tier2):
                c['tier_two'] = 'No tickets available'
            else:
                c['tier_two'] = int(event_tier2)-int(ticket_tier2)
                d.append((2000,'Tier 2'))
            
        if event_tier3:
            ticket_tier3 = ticket.filter(event_tier =3).count()
            if int(ticket_tier3) >= int(event_tier3):
                c['tier_three'] = 'No tickets available'
            else:
                c['tier_three'] = int(event_tier3)-int(ticket_tier3)
                d.append((3000,'Tier 3'))
            
    except Event.DoesNotExist:
        raise Http404
    if request.method == "POST":
        form_tier1 = SalePaymentForm(request.POST,menu=d)    
        
        if form_tier1.is_valid(): # charges the card
            
#        ticket_form_set = modelformset_factory(Ticket, fields =('event_id', 'event_tier',
#                                        'balanced_uri', 'card_name', 'card_four_digits'))
#        ticket_form = ticket_form_set(request.POST)
#        if ticket_form.is_valid():
            tier_num = '0'
            if form_tier1.cleaned_data['price'] == '1000' :tier_num = '1'
            if form_tier1.cleaned_data['price'] == '2000' :tier_num = '2'
            if form_tier1.cleaned_data['price'] == '3000' :tier_num = '3' 
            ticket = Ticket(holder = request.user,
                            event_id = event,
                            event_tier = tier_num,
                            balanced_uri = form_tier1.cleaned_data['uri'],
                            card_name = form_tier1.cleaned_data['name'],
                            card_four_digits = str(form_tier1.cleaned_data['number'])[:4],
                            )
            ticket.save()


            event.update_funding_status()

            if event.funding_status == 100 and not event.is_tipped:
                process_event.delay(event)
                
                #pass
                
                

            # email user: thank you for your support
            if request.user.email:
                message ='Dear '+ request.user.username +',\n'+'Thanks a ton for your support towards the event "'+ event.evname +'".\n'+'Have an awesome time there. You deserve it.\n Your credit card will not be charged until the event has tipped. \n -Revel Raise'
                email = EmailMessage('Event Ticket - '+ event.evname, message, to=[request.user.email])
                email.send()
#            request.user.email_user('Ravel Raise Ticket purchase:Thank you',message)

            if event.is_tipped:
                ticket.charge()
                if request.user.email:
                    message ='Dear '+ request.user.username +',\n The' + event.evname + 'event you signed up for is now Tipped.\
                     Which means The event is on!!! Your credit card will be charged shortly.\n Hope you have an awesome time there.\n -Revel Raise.'
                    email = EmailMessage('Event Ticket - '+ event.evname, message, to=[request.user.email])
                    email.send()
    #                request.user.email_user('Ravel Raise Ticket purchase:Thank you',message)

            user = request.user
            redir = '/%s/?w=true' % user
            data = [{'redir': redir}]
            return HttpResponse(json.dumps(data))
            #return redirect('/accounts/%s/?w=true' % user)
    else:
        form_tier1 = SalePaymentForm(menu=d)
        if request.user.is_authenticated() and Ticket.objects.filter(holder_id=request.user, status ='U',event_id_id=event_id):
            c['ticket_remove'] = True
        #page-view counter
        try:
            EventView = EventViews.objects.get(event_id_id=event_id)
            EventViews.objects.filter(event_id_id=event_id).update(count= F("count") + 1)
        except ObjectDoesNotExist:  
            EventView = EventViews()      
            EventView.event_id = event
            EventView.count = 1
            EventView.save()
        names_list = {}
        money_raised = ''
        tier_tally = {}

        if request.user.is_authenticated() and request.user == event.creator:
            sum = 0
            tier_tally['tier_1'] = 0
            tier_tally['tier_2'] = 0
            tier_tally['tier_3'] = 0
            
            for each in Ticket.objects.filter(event_id_id=event_id):
                names_list[each.holder.first_name + " " + each.holder.last_name] = each.event_tier
                sum = sum + float(event.get_tier_price(each.event_tier))
                if each.event_tier == "1":
                    tier_tally['tier_1'] = tier_tally['tier_1'] + 1
                elif each.event_tier == "2":
                    tier_tally['tier_2'] = tier_tally['tier_2'] + 1
                elif each.event_tier == "3":
                    tier_tally['tier_3'] = tier_tally['tier_3'] + 1
                    
            money_raised = sum
            
    return render_to_response("events/detail.html",c,
                    RequestContext( request, {'form': form_tier1, 'event':event, 
                                              'names':names_list, 'money':money_raised, 'tier_tally':tier_tally}) )


def createanevent(request):
    logged_user = request.user
    if request.method == 'POST':
        form = EventForm(request.POST, request.FILES)

        if form.is_valid():
            # If form has passed all validation checks then continue to save member.
            tier_one = add_gateway_comission(form.cleaned_data['tier_one']) 
            tier_one_post_tipp = add_gateway_comission(form.cleaned_data['tier_one_post_tipp'])
            
            tier_two = add_gateway_comission(form.cleaned_data['tier_two'])
            tier_two_post_tipp = add_gateway_comission(form.cleaned_data['tier_two_post_tipp'])
            
            tier_three = add_gateway_comission(form.cleaned_data['tier_three']) 
            tier_three_post_tipp = add_gateway_comission(form.cleaned_data['tier_three_post_tipp'])
            
            try:
                event = Event(creator=request.user, 
                              evname=form.cleaned_data['evname'], 
                              descr=form.cleaned_data['descr'],
                               
                              tier_one = str(tier_one), 
                              tier_one_desc = form.cleaned_data['tier_one_desc'],
                              tier_one_tickets = form.cleaned_data['tier_one_tickets'], 
                              tier_one_post_tipp = str(tier_one_post_tipp),
                              
                              tier_two = str(tier_two), 
                              tier_two_desc = form.cleaned_data['tier_two_desc'],
                              tier_two_tickets = form.cleaned_data['tier_two_tickets'], 
                              tier_two_post_tipp = str(tier_two_post_tipp),
                              
                              tier_three = str(tier_three),  
                              tier_three_desc=form.cleaned_data['tier_three_desc'],
                              tier_three_tickets = form.cleaned_data['tier_three_tickets'], 
                              tier_three_post_tipp = str(tier_three_post_tipp), 
                              
                              location=form.cleaned_data['location'], 
                              address=form.cleaned_data['address'], 
                              phone=form.cleaned_data['phone'], 
                              website=form.cleaned_data['website'], 
                              pic=form.cleaned_data['pic'], 
                              vid_embed=form.cleaned_data['vid_embed'],
                              tipp_point = str(add_rr_comission(form.cleaned_data['tipp_point'])),
                              last_date = form.cleaned_data['last_date'])
                event.save()
            except:
                return HttpResponse("error")
            # Save is done redirect member to logged in page.
            return redirect('/event/%s' % event.id)
        else:
            # If form is NOT valid then show the registration page again.
            form = EventForm() 
            context = {'form':form}
            return render_to_response('/addevent.html', context,context_instance=RequestContext(request))


def add_gateway_comission(value):
    if not value: return value
    params = SingletonSiteParams.objects.get(pk =1)
    if not params: return value
    return round(float(value)/(1 - (params.payment_gateway_comission/100)),2)

def add_rr_comission(value):
    if not value: return value
    params = SingletonSiteParams.objects.get(pk =1)
    if not params: return value
    return round(float(value)/(1 -(params.ravel_raise_comission/100)), 2)



def deleteticket(request,event_id):
    event = Event.objects.get(pk=event_id)
    ticket = Ticket.objects.all()
    tics = Ticket.objects.filter(holder_id=request.user, event_id = event, status ='U')
    if tics:
        tics.delete()
        event.update_funding_status()
        return HttpResponseRedirect('/events/'+event_id+'')
