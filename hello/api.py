from tastypie.resources import ModelResource
from tastypie.constants import ALL
from events.models import Event

class EntryResource(ModelResource):
    class Meta:
        queryset = Event.objects.all()
        resource_name = 'events'