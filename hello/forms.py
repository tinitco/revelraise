from django import forms

class SignupForm(forms.Form):
    first_name = forms.CharField(max_length=30, label='First Name')
    last_name = forms.CharField(max_length=30, label='Last Name')

    def save(self, user):
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.save()


from events.models import Event
        
from django_summernote.widgets import SummernoteWidget, SummernoteInplaceWidget
from bootstrap3_datetime.widgets import DateTimePicker

class EventForm(forms.ModelForm):
    evname = forms.CharField(required=True,widget=forms.TextInput(
                                        attrs={'placeholder':'',}))
    descr = forms.CharField(required=True,widget=SummernoteWidget())
    tier_one = forms.CharField(required=True,widget=forms.TextInput(
                                        attrs={'placeholder':'$',}))
    tier_one_post_tipp = forms.CharField(required=False,widget=forms.TextInput(
                                        attrs={'placeholder':'$',}))
    tier_one_desc = forms.CharField(required=True,widget=forms.TextInput(
                                        attrs={'placeholder':'Description',}))
    tier_one_tickets = forms.CharField(required=True,widget=forms.TextInput(
                                        attrs={'placeholder':'# of tickets',}))
    tier_two = forms.CharField(required=False,widget=forms.TextInput(
                                        attrs={'placeholder':'$',}))
    tier_two_post_tipp = forms.CharField(required=False,widget=forms.TextInput(
                                        attrs={'placeholder':'$',}))
    tier_two_desc = forms.CharField(required=False,widget=forms.TextInput(
                                        attrs={'placeholder':'Description',}))
    tier_two_tickets = forms.CharField(required=False,widget=forms.TextInput(
                                        attrs={'placeholder':'# of tickets',}))
    tier_three = forms.CharField(required=False,widget=forms.TextInput(
                                        attrs={'placeholder':'$',}))
    tier_three_post_tipp = forms.CharField(required=False,widget=forms.TextInput(
                                        attrs={'placeholder':'$',}))
    tier_three_desc = forms.CharField(required=False,widget=forms.TextInput(
                                        attrs={'placeholder':'Description',}))
    tier_three_tickets = forms.CharField(required=False,widget=forms.TextInput(
                                        attrs={'placeholder':'# of tickets',}))
    location = forms.CharField(required=True,widget=forms.TextInput(
                                        attrs={'placeholder':'',}))
    location_route = forms.CharField(required=True,widget=forms.TextInput(
                                        attrs={'placeholder':'asdf',}))
    phone = forms.CharField(required=False,widget=forms.TextInput(
                                        attrs={'placeholder':'',}))
    website = forms.CharField(required=False, widget=forms.TextInput(
                                        attrs={'placeholder':'',}))
    tipp_point = forms.CharField(required=True, widget=forms.TextInput(
                                        attrs={'placeholder':'$',}))
    #last_date = forms.DateField(required=True, widget=SelectDateWidget(
                            #attrs={'placeholder':'$',}), initial=datetime.date.today())
    last_date = forms.DateField(widget=DateTimePicker(options={"format": "YYYY-MM-DD",
                                       "pickTime": True}))
    vid_embed = forms.CharField(required=False, widget=forms.TextInput(
                                        attrs={'placeholder':'<iframe>/<embed>',}))

    class Meta:
        model = Event

        fields = ('evname', 'descr', 'tier_one', 'tier_one_desc', 'tier_two',\
                   'tier_two_desc', 'tier_three', 'tier_three_desc', 'location',\
                   'address', 'phone', 'website', 'pic', 'vid_embed',\
                   'tier_one_post_tipp', 'tier_two_post_tipp', 'tier_three_post_tipp',\
                   'tipp_point', 'last_date')
        
        def clean_evname(self):
            evname = self.cleaned_data['evname']
            descr = self.cleaned_data['descr']
            #tier_one = self.cleaned_data['tier_one']
            #tier_one_desc = self.cleaned_data['tier_one_desc']
            #tier_two = self.cleaned_data['tier_two']
            #tier_two_desc = self.cleaned_data['tier_two_desc']
            #tier_three = self.cleaned_data['tier_three']
            #tier_three_desc = self.cleaned_data['tier_three_desc']
            location = self.cleaned_data['location']
            address = self.cleaned_data['address']
            phone = self.cleaned_data['phone']
            website = self.cleaned_data['website']
            pic = self.cleaned_data['pic']
            vid_embed = self.cleaned_data['vid_embed']

            try:
                    Event.objects.get(evname=evname)
            except Event.DoesNotExist:
                    return evname, descr, tier_one, tier_one_desc,\
                         tier_two, tier_two_desc, tier_three, tier_three_desc,\
                         location, address, phone, website, tier_one_post_tipp,\
                         tier_two_post_tipp, tier_three_post_tipp, tipp_point, last_date
            raise forms.ValidationError("Event already exists.")
