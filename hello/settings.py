    # Django settings for hello project.

DEBUG = True
TEMPLATE_DEBUG = DEBUG

import os

ADMINS = (
    ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', #django.db.backends.mysql
        'NAME': 'revelraise', 
        'USER': 'root', #  change to your own username and password
        'PASSWORD': 'frodo',
        'HOST': '',
        'PORT': '',
    }
}
# Django settings for project project.


BASE_DIR = os.path.dirname(os.path.abspath(__file__))



SITE_ROOT = os.path.dirname(os.path.realpath(__file__))

import os.path
root = os.path.dirname(__file__).replace('\\','/')
 

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = []

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'America/New_York'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/var/www/example.com/static/"
#STATIC_ROOT = '/static/'

# URL prefix for static files.
# Example: "http://example.com/static/", "http://static.example.com/"
#STATIC_URL = '/static/'

#######LOCAL ENVIRONMENT VARIABLES######
MEDIA_ROOT =os.path.join(BASE_DIR, "../static")#os.path.join(os.path.dirname(__file__), 'media')
            #'/Users/neildesh/src/tapcard/static/'
MEDIA_URL = '/media/'
STATIC_ROOT = ''#os.path.join(os.path.dirname(__file__), '../static')#'/static/'
STATIC_URL = '/static/'
# Additional locations of static files
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "../static"),
    #'/Users/neildesh/src/tapcard/static/',
    #os.path.join(os.path.dirname(__file__), '../static'),
)
FILE_UPLOAD_PERMISSIONS = 0644
########################################


#MEDIA_ROOT = '/home/ubuntu/hello/static/'
#MEDIA_URL = ''
#STATIC_ROOT = '/home/ubuntu/static/'
#STATIC_URL = '/static/'
#STATICFILES_DIRS = (
#    os.path.join(BASE_DIR, "static"),
#    '/etc/home/ubuntu/hello/static/',
#)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    #'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'gxpx^i*su2th#7j_5#5zp6zd&^+kcy5&3-24=s+!12s_g5-hw_'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'lockdown.middleware.LockdownMiddleware',
)

ROOT_URLCONF = 'hello.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'hello.wsgi.application'

TEMPLATE_DIRS = (
    [os.path.join(BASE_DIR, 'templates')]
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    #'django.contrib.gis',
    #'django.contrib.gis.geos',
    'events',
    #'easy_thumbnails',
    'django.contrib.humanize',
    # Uncomment the next line to enable the admin:
     'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    'django.contrib.admindocs',
    'lockdown',
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    # ... include the providers you want to enable:
    'allauth.socialaccount.providers.facebook',
    'allauth.socialaccount.providers.google',
    'allauth.socialaccount.providers.twitter',
    'djcelery',
    'kombu.transport.django',
    'ajaxuploader',
    'django_summernote',
    #'singleton_models',
    #'south',
    'bootstrap3_datetime'
)


LOGIN_REDIRECT_URL = '/'
LOCKDOWN_PASSWORDS = ('07059', )
#LOCKDOWN_FORM = 'lockdown.forms.CustomLockdownForm'



BALANCED_KEY = 'ak-test-EdiVk8p2RkkNnXOGqkrTP9JdMRFhYIZ9'
BALANCED_URL = 'https://api.balancedpayments.com'
BALANCED_STATEMENT_TEXT = 'REVELRAISE'
BALANCED_MARKET = 'TEST-MP3BUTNlzLlEQq7B5TB5fpq0'



TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.request',
    'django.contrib.messages.context_processors.messages',
    "allauth.account.context_processors.account",
    "allauth.socialaccount.context_processors.socialaccount",
    #'payments.context_processors.payments_settings'
)



AUTHENTICATION_BACKENDS = (
    # Needed to login by username in Django admin, regardless of `allauth`
    "django.contrib.auth.backends.ModelBackend",

    # `allauth` specific authentication methods, such as login by e-mail
    "allauth.account.auth_backends.AuthenticationBackend",
)

ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_AUTHENTICATION_METHOD = "email"
ACCOUNT_UNIQUE_EMAIL = True
ACCOUNT_USERNAME_REQUIRED = False


ACCOUNT_SIGNUP_FORM_CLASS = 'hello.forms.SignupForm'

import djcelery
djcelery.setup_loader()
BROKER_URL = "django://" 



# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}


GEOIP_PATH = os.path.join(BASE_DIR, "/static/") 


# email settings Django uses to send.
EMAIL_USE_TLS = True
EMAIL_HOST='smtp.gmail.com'
EMAIL_PORT=587#25
EMAIL_HOST_USER='revelraise@gmail.com'#username'
EMAIL_HOST_PASSWORD='1396sdsp'#password'





SUMMERNOTE_CONFIG = {
    # Using SummernoteWidget - iframe mode
    'iframe': False,  # or set False to use SummernoteInplaceWidget - no iframe mode

    # Change editor size
    'width': '100%',
    'height': '340',

    # Set editor language/locale
    'lang': 'en-US',

    # Customize toolbar buttons
    'toolbar': [
        ['style', ['style']],
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['para', ['ul', 'ol', 'height']],
        ['insert', ['link']],
    ],

    # Set `upload_to` function for attachments.
    #'attachment_upload_to': my_custom_upload_to_func(),

    # Set custom storage class for attachments.
    #'attachment_storage_class': 'my.custom.storage.class.name',
}