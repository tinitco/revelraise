from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()
from hello import views

from api import EntryResource
whatever_resource = EntryResource()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include(whatever_resource.urls)),
    url(r'^$', views.index, name='index'),
    url(r'^browse/', views.browse, name = "browse"),
    url(r'^addevent/$', views.addevent, name='addevent'),
    url(r'^event/', include('events.urls', namespace = "events")),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^isloggedin/$', views.is_user_logged_in, name='checkLogin'),
    #url(r'^profile/$', views.profile, name='profile'),
    url(r'^about/', views.about, name='about'),
    url(r'^faq/', views.faq, name='faq'),
    url(r'^jobs/', views.jobs, name='jobs'),
    url(r'^contact/', views.contact, name='contact'),
    url(r'^terms/', views.terms, name='terms'),
    url(r'^(?P<username>\w+)/$', views.profile, name='profile'),
    # Uncomment the admin/doc line below to enable admin documentation:
    #url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
    url(r'start$', views.start, name="start"),
    url(r'ajax-upload$', views.import_uploader, name="my_ajax_upload"),
    url(r'^summernote/', include('django_summernote.urls')),
)
