from django.http import HttpResponse
from django.template import RequestContext, loader
from django.http import Http404
from django.shortcuts import render

from events.models import Event,Ticket

from allauth.account.forms import LoginForm, SignupForm

from events.models import Event, Sale, EventViews

from forms import EventForm

from django.contrib.gis.geoip import GeoIP



from django.middleware.csrf import get_token
from django.shortcuts import render_to_response
from django.template import RequestContext

from ajaxuploader.views import AjaxFileUploader


def start(request):
    csrf_token = get_token(request)
    return render_to_response('import.html',
        {'csrf_token': csrf_token}, context_instance = RequestContext(request))

import_uploader = AjaxFileUploader()



def index(request):
    form_class = LoginForm
    signup_form = SignupForm
    template = loader.get_template('home/index.html')

    events_upcoming = Event.objects.filter(status = 'A').order_by('last_date')[:8]
    events_funded = Event.objects.filter(status = 'A').order_by('-funding_status')[:8] #100% to 0%
    events_pop = Event.objects.filter(status = 'A').order_by('views')[:8] #page views events_eventviews
    events_recent = Event.objects.filter(status = 'A').order_by('-id')[:8] 

    context = RequestContext(request, {
        'events_upcoming': events_upcoming,
        'events_funded': events_funded,
        'events_pop': events_pop,
        'events_recent': events_recent,
        'form': form_class,
        'signup_form': signup_form
    })
    return HttpResponse(template.render(context))

def browse(request):
    form_class = LoginForm
    signup_form = SignupForm

    location = {}
    ip = get_client_ip(request)
    location["ip"] = ip
    if ip:
        g = GeoIP("/var/www/revelraise/static/")
        city_name = g.city(ip)
        location["city"] = city_name['city']
        location["region"] = city_name['region']
        
        if city_name and city_name['city']:
            events_near = Event.objects.filter(status = 'A',address__contains = city_name['city'])[:5] #order_by('-pub_date')[:5]
        
    city = request.GET.get('city', '')
    venue = request.GET.get('venue', '')

    events_near = Event.objects.filter(status = 'A').order_by('id')[:8] #order_by('-pub_date')[:5]
    events_funded = Event.objects.filter(status = 'A', address__contains = city, location__contains= venue).order_by('-funding_status')[:8]
    events_pop = Event.objects.filter(status = 'A', address__contains = city, location__contains= venue).order_by('views')[:8]
    events_recent = Event.objects.filter(status = 'A', address__contains = city, location__contains= venue).order_by('-id')[:8]
    
    cities = Event.objects.filter(status = 'A').values_list('address', flat=True)
    city_list =[]
    for each in cities:
        city_list.append(each.split(',')[-3])
    city_list = list(set(city_list))
    
    venues = Event.objects.filter(status = 'A').values_list('location', flat=True)
    venues = list(set(venues))
    context = RequestContext(request, {
        'location': location,
        'events_near': events_near,
        'events_funded': events_funded,
        'events_pop': events_funded,
        'events_recent': events_recent,
        'city_list': city_list,
        'venue_list': venues,
        'form': form_class,
        'signup_form': signup_form,
        'city_filter': city,
        'venue_filter': venue
    })
    template = loader.get_template('home/browse.html')
    return HttpResponse(template.render(context))

def get_client_ip(request):
    try:
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    except:
        ip = "unknown"
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[-1].strip()
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip



def addevent(request):
    form_class = LoginForm
    signup_form = SignupForm
    template = loader.get_template('home/addevent.html')
    event_form = EventForm()    

    context = RequestContext(request, {
        'event_form': event_form,
        'form': form_class,
        'signup_form': signup_form
    })
    return HttpResponse(template.render(context))    
    

def profile(request, username):
    form_class = LoginForm
    signup_form = SignupForm
    template = loader.get_template('account/profile.html')

    tics = Ticket.objects.values_list('event_id',flat=True).filter(holder= request.user)
    events_going = Event.objects.filter(pk__in = tics) #order_by('-pub_date')[:5]
    t = Ticket.objects.filter(holder= request.user)
    
    events_created = Event.objects.filter(creator_id=request.user)

    context = RequestContext(request, {
        'events_going': events_going,
        'tickets': t,
        'events_created': events_created,
        'form': form_class,
        'signup_form': signup_form

    })
    return HttpResponse(template.render(context))

def is_user_logged_in(request):
    if request.user.is_authenticated():
        return HttpResponse(status =200)
    raise Http404


def about(request):
    form_class = LoginForm
    signup_form = SignupForm
    template = loader.get_template('home/info/about.html')

    context = RequestContext(request, {
        'form': form_class,
        'signup_form': signup_form
    })
    return HttpResponse(template.render(context))

def faq(request):
    form_class = LoginForm
    signup_form = SignupForm
    template = loader.get_template('home/info/faq.html')

    context = RequestContext(request, {
        'form': form_class,
        'signup_form': signup_form
    })
    return HttpResponse(template.render(context))

def jobs(request):
    form_class = LoginForm
    signup_form = SignupForm
    template = loader.get_template('home/faq.html')

    context = RequestContext(request, {
        'form': form_class,
        'signup_form': signup_form
    })
    return HttpResponse(template.render(context))

def contact(request):
    form_class = LoginForm
    signup_form = SignupForm
    template = loader.get_template('home/faq.html')

    context = RequestContext(request, {
        'form': form_class,
        'signup_form': signup_form
    })
    return HttpResponse(template.render(context))

def terms(request):
    form_class = LoginForm
    signup_form = SignupForm
    template = loader.get_template('home/faq.html')

    context = RequestContext(request, {
        'form': form_class,
        'signup_form': signup_form
    })
    return HttpResponse(template.render(context))


"""
def landing(request):
    form_class = LoginForm
    signup_form = SignupForm
    template = loader.get_template('home/landing.html')
    events_near = Event.objects.all().order_by('-id')[:4]

    context = RequestContext(request, {
        'events': events_near,
        'form': form_class,
        'signup_form': signup_form
    })
    return HttpResponse(template.render(context))
"""