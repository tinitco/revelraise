"""
WSGI config for hello project.

This module contains the WSGI application used by Django's development server
and any production WSGI deployments. It should expose a module-level variable
named ``application``. Django's ``runserver`` and ``runfcgi`` commands discover
this application via the ``WSGI_APPLICATION`` setting.

Usually you will have the standard Django WSGI application here, but it also
might make sense to replace the whole Django WSGI application with a custom one
that later delegates to the Django one. For example, you could introduce WSGI
middleware here, or combine a Django application with an application of another
framework.

"""
import os
import django.core.handlers.wsgi
# We defer to a DJANGO_SETTINGS_MODULE already in the environment. This breaks
# if running multiple sites in the same mod_wsgi process. To fix this, use
# mod_wsgi daemon mode with each site in its own daemon process, or use
# os.environ["DJANGO_SETTINGS_MODULE"] = "hello.settings"
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hello.settings")

#sys.path.append('/home/ubuntu/revelraise')

# This application object is used by any WSGI server configured to use this
# file. This includes Django's development server, if the WSGI_APPLICATION
# setting points here.
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

# Apply WSGI middleware here.
# from helloworld.wsgi import HelloWorldApplication
# application = HelloWorldApplication(application)
#_application = django.core.handlers.wsgi.WSGIHandler()
#from hello.settings import URL_PREFIX
#def application(environ, start_response):
#    environ['SCRIPT_NAME'] = '/' + URL_PREFIX[:-1]
#    environ['PATH_INFO'] = environ['SCRIPT_NAME'] + environ['PATH_INFO']
#    return _application(environ, start_response)